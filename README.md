# Senior Developer Refactoring Excercise

This codebase forms the baseline for a refactoring discussion.
The intended usage is to give the Interviewee 10-20 Minutes to 
familiarize with `Application.java` and `SqlRequest.java` and
then have a 10 Minute discussion about refactoring it, lead by 
the questions in `Application.java`'s head javadoc.

* This is not a Test!
* This is supposed to be done in casual conversation!
