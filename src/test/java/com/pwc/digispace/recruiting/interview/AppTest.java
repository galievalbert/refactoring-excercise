package com.pwc.digispace.recruiting.interview;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.pwc.digispace.recruiting.interview.Application.User;

public class AppTest {

	@Test
	public void userFieldsCheck() {
		String name = "Adam";
		String birthDay = "01.05.1980";
		int salary = 85000;
		String department = "Marketing";
		
		User user = new User(name, birthDay, salary, department);
		
		assertEquals(user.name, name);
		assertEquals(user.birth, birthDay);
		assertEquals(user.salary.intValue(), salary);
		assertEquals(user.department, department);
	}
}
